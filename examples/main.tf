module "iam_role-NAME" {
  source             = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-iam-role.git?ref=master"
  name               = "NAME"
  assume_role_policy = "${file("files/k8s.saudeprotect.com.br-backup_assume_role_policy.json")}"
  policy             = "${file("files/k8s.saudeprotect.com.br-backup_policy.json")}"
}
